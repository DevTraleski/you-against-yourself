﻿using UnityEngine;
using System.Collections;

public class G_VisionBlock : MonoBehaviour {

	public float timeCooldown;
	private bool skillAvailable;
	public float timeActivated;

	// Use this for initialization
	void Start () 
	{
		skillAvailable = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Enable()
	{
		if (skillAvailable) 
		{
			StartCoroutine (ActivateTime ());
		}
		
	}
	
	IEnumerator ActivateTime()
	{
		skillAvailable = false;
		Debug.Log ("VisionBlock");
		
		yield return new WaitForSeconds (timeActivated);
		
		Debug.Log ("Stop VisionBlock");
		StartCoroutine (Cooldown());
		
	}
	
	IEnumerator Cooldown()
	{
		yield return new WaitForSeconds (timeCooldown);
		
		Debug.Log ("finished cooldown VisionBlock");
		skillAvailable = true;
	}
}
