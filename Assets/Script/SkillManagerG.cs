﻿using UnityEngine;
using System.Collections;

public class SkillManagerG : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (Input.GetButtonDown ("Ghost_Skill1")) 
		{
			this.GetComponent<G_VisionBlock>().Enable();
		}
		else if (Input.GetButtonDown ("Ghost_Skill2")) 
		{
			this.GetComponent<G_BuffEnemy>().Enable();
		}
		else if (Input.GetButtonDown ("Ghost_Skill3")) 
		{
			this.GetComponent<G_SlowPlayer>().Enable();
		}
	}
}
