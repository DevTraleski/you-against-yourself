﻿using UnityEngine;
using System.Collections;

public class ActivatePowerUp : MonoBehaviour {

	private int gunNum;
	private GameObject Player;

	// Use this for initialization
	void Start () 
	{
		Player = GameObject.Find ("Player");
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void SelectPowerUp()
	{
		gunNum = Random.Range (0, 3);

		if (gunNum == 0)
		{
			Player.GetComponent<WeaponManager>().fireRate = 0.3f;
			Player.GetComponent<WeaponManager>().currentGun = "Pistol";
		
		}
		if(gunNum == 1)
		{
			Player.GetComponent<WeaponManager>().fireRate = 0.1f;
			Player.GetComponent<WeaponManager>().currentGun = "MachineGun";
		}
		else if(gunNum == 2)
		{
			Player.GetComponent<WeaponManager>().fireRate = 0.5f;
			Player.GetComponent<WeaponManager>().currentGun = "Shotgun";
		}

	}


	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "player") 
		{
			SelectPowerUp();
			Destroy(this.gameObject);
		}
	}
}
