﻿using UnityEngine;
using System.Collections;

public class MovementBullet : MonoBehaviour {

	private GameObject player;
	public float speed = 5;
	private Vector3 side;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.Find ("Player");

		if (player.GetComponent<MovementPlayer>().direction) 
		{
			side = Vector3.right;
		} 
		else 
		{
			side = Vector3.left;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		Vector3 cameraPos = Camera.main.WorldToScreenPoint (transform.position);
		

		
		
		if (cameraPos.x < 0)
			Destroy (this.gameObject);
		
		if (cameraPos.x > Screen.width)
			Destroy (this.gameObject);
		
		if (cameraPos.y < 0)
			Destroy (this.gameObject);
		
		if (cameraPos.y > Screen.height)
			Destroy (this.gameObject);

		this.transform.Translate (side * speed);
	}
}
