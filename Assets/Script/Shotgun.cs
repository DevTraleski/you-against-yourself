﻿using UnityEngine;
using System.Collections;

public class Shotgun : MonoBehaviour 
{

	public GameObject SpawnBulletRight;

	public GameObject SpawnBulletLeft;

	public GameObject Bullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Fire(bool direction)
	{
		if (direction) 
		{
			Instantiate (Bullet,new Vector3(SpawnBulletRight.transform.position.x,SpawnBulletRight.transform.position.y+0.5f,SpawnBulletRight.transform.position.z),
			             Quaternion.Euler(SpawnBulletRight.transform.rotation.x,SpawnBulletRight.transform.rotation.y,SpawnBulletRight.transform.rotation.z+3));
			Instantiate (Bullet,
			             SpawnBulletRight.transform.position,
			             SpawnBulletRight.transform.rotation);
			Instantiate (Bullet,
			             new Vector3(SpawnBulletRight.transform.position.x,SpawnBulletRight.transform.position.y-0.5f,SpawnBulletRight.transform.position.z),
			             Quaternion.Euler(SpawnBulletRight.transform.rotation.x,SpawnBulletRight.transform.rotation.y,SpawnBulletRight.transform.rotation.z-3));
		} 
		else 
		{
			
			Instantiate (Bullet,new Vector3(SpawnBulletLeft.transform.position.x,SpawnBulletLeft.transform.position.y-0.5f,SpawnBulletLeft.transform.position.z),
			             Quaternion.Euler(SpawnBulletLeft.transform.rotation.x,SpawnBulletLeft.transform.rotation.y,SpawnBulletLeft.transform.rotation.z+3));
			Instantiate (Bullet,
			             SpawnBulletLeft.transform.position,
			             SpawnBulletLeft.transform.rotation);
			Instantiate (Bullet,
			             new Vector3(SpawnBulletLeft.transform.position.x,SpawnBulletLeft.transform.position.y+0.5f,SpawnBulletLeft.transform.position.z),
			             Quaternion.Euler(SpawnBulletLeft.transform.rotation.x,SpawnBulletLeft.transform.rotation.y,SpawnBulletLeft.transform.rotation.z-3));
		}


	}


}