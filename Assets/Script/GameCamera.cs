﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {

    private GameObject player;

	// Use this for initialization
	void Start () 
    {
        player = GameObject.Find("Player");
	}
	
	// Update is called once per frame
	void FixedUpdate() 
    {
        float posX = CalculateSidescroller(transform.position.x, player.transform.position.x);
        float posY = CalculateSidescroller(transform.position.y, player.transform.position.y);

        transform.position = new Vector3(posX, posY, transform.position.z);
	}

    private float CalculateSidescroller(float cameraPos,float otherPos)
    {
        float dir = Mathf.Sign(otherPos - cameraPos);

        if(dir != Mathf.Sign(otherPos - cameraPos))
        {
            return cameraPos;
        }
        else
        {
            return otherPos;
        }
    }
}
