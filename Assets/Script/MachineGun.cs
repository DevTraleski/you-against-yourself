﻿using UnityEngine;
using System.Collections;

public class MachineGun : MonoBehaviour {

	public GameObject SpawnBulletRight;
	public GameObject SpawnBulletLeft;
	public GameObject Bullet;

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void Fire(bool direction)
	{


		if(direction)
		{
			Instantiate(Bullet,
			            SpawnBulletRight.transform.position,
			            SpawnBulletRight.transform.rotation);
		}
		else
		{
			Instantiate(Bullet,
			            SpawnBulletLeft.transform.position,
			            SpawnBulletLeft.transform.rotation);
		}
	
	}
	
}
