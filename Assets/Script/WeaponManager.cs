﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour 
{
	public string currentGun;
	private bool isFire;
	public float fireRate;
	

	// Use this for initialization
	void Start () 
	{
		currentGun = "Pistol";
		isFire = true;
		fireRate = 0.3f;
	
	}
	
	// Update is called once per frame

	void FixedUpdate () 
	{
		
		
		//FirePlayer
		if (Input.GetButton("Fire_Player") && isFire) 
		{
			StartCoroutine(SpeedFire());
			
			if(currentGun.Equals("Pistol"))
			{
				this.GetComponent<Pistol>().Fire(this.GetComponent<MovementPlayer>().direction);
			}
			else if(currentGun.Equals("MachineGun"))
			{
				this.GetComponent<MachineGun>().Fire(this.GetComponent<MovementPlayer>().direction);
			}
			else if(currentGun.Equals("Shotgun"))
			{
				this.GetComponent<Shotgun>().Fire(this.GetComponent<MovementPlayer>().direction);
			}
			
			
			
			isFire = false;
		}
		//FirePlayer
		
		
	}
	
	
	
	//Speed for shoot bullet
	IEnumerator SpeedFire()
	{
		yield return new WaitForSeconds(fireRate);
		
		isFire = true;
	}
	//Speed for shoot bullet
}
