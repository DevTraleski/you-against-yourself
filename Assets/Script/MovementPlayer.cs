﻿using UnityEngine;
using System.Collections;

public class MovementPlayer : MonoBehaviour 
{

	public float speed;
	public bool direction;
	public float jumpHeight;
	public float lineJump = 1;

	private Animator playerAnim;
	private bool isJumping;
	private Rigidbody2D rb2D;

	// Use this for initialization
	void Start () 
	{
	
		rb2D = this.gameObject.GetComponent<Rigidbody2D>();
		isJumping = false;
		playerAnim = GetComponent<Animator> ();
		direction = true;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		//Movement Player
		if (Input.GetAxis("Horizontal_Player") > 0) 
		{
			transform.Translate(Vector3.right * Time.deltaTime * speed);
			playerAnim.SetBool("side", true);
			direction = true;
			
		}
		if (Input.GetAxis("Horizontal_Player") < 0) 
		{
			transform.Translate(Vector3.left * Time.deltaTime * speed);
			playerAnim.SetBool("side", false);
			direction = false;
		}
		
		if(Input.GetAxis("Vertical_Player") > 0 && !isJumping)
		{
			rb2D.velocity = new Vector2(rb2D.velocity.x,jumpHeight);
			isJumping = true;
		}
		//Movement Player

		//Raycast For Jump
		RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, lineJump);
		Debug.DrawLine(transform.position,transform.position-(Vector3.up*lineJump));
		
		if (hit.collider != null) 
		{
			
			if(hit.collider.gameObject.tag == "place")
			{
				isJumping=false;
				
			}
		}
		//Raycast For Jump
	}
}
