﻿using UnityEngine;
using System.Collections;

public class MovementGhost : MonoBehaviour {

	public float speed;
	private Animator GhostAnim;

	// Use this for initialization
	void Start () 
	{
		GhostAnim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		LimitScreen ();
		if (Input.GetAxis ("Horizontal_Ghost") > 0) 
		{
			transform.Translate(Vector3.right * Time.deltaTime * speed);
			GhostAnim.SetBool("side",false);
		}
		if (Input.GetAxis ("Horizontal_Ghost") < 0) 
		{
			transform.Translate(Vector3.left * Time.deltaTime * speed);
			GhostAnim.SetBool("side",true);
		}
		if (Input.GetAxis ("Vertical_Ghost") > 0) 
		{
			transform.Translate(Vector3.up * Time.deltaTime * speed);
		}
		if (Input.GetAxis ("Vertical_Ghost") < 0) 
		{
			transform.Translate(Vector3.down * Time.deltaTime * speed);
		}




	}

	void LimitScreen()
	{
		Vector3 cameraPos = Camera.main.WorldToScreenPoint (gameObject.transform.position);
		
		Vector3 leftScreenLimit = Camera.main.ScreenToWorldPoint (new Vector3 (0, cameraPos.y,cameraPos.z));
		Vector3 rightScreenLimit = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, cameraPos.y,cameraPos.z));
		Vector3 UpScreenLimit = Camera.main.ScreenToWorldPoint (new Vector3 (cameraPos.x, 0,cameraPos.z));
		Vector3 DownScreenLimit = Camera.main.ScreenToWorldPoint (new Vector3 (cameraPos.x, Screen.height,cameraPos.z));

		leftScreenLimit = new Vector3 (leftScreenLimit.x + this.gameObject.GetComponent<BoxCollider2D> ().size.x/2, leftScreenLimit.y, leftScreenLimit.z);
		rightScreenLimit = new Vector3 (rightScreenLimit.x - this.gameObject.GetComponent<BoxCollider2D> ().size.x/2, rightScreenLimit.y, rightScreenLimit.z);
		UpScreenLimit = new Vector3 (UpScreenLimit.x, UpScreenLimit.y + this.gameObject.GetComponent<BoxCollider2D>().size.y/2, UpScreenLimit.z);
		DownScreenLimit = new Vector3 (DownScreenLimit.x, DownScreenLimit.y - this.gameObject.GetComponent<BoxCollider2D>().size.y/2, DownScreenLimit.z);

		Vector3 leftWorldLimit = Camera.main.WorldToScreenPoint (leftScreenLimit);
		Vector3 rightWorldLimit = Camera.main.WorldToScreenPoint (rightScreenLimit);
		Vector3 upWorldLimit = Camera.main.WorldToScreenPoint (UpScreenLimit);
		Vector3 downWorldLimit = Camera.main.WorldToScreenPoint (DownScreenLimit);

		if (cameraPos.x < leftWorldLimit.x)
			gameObject.transform.position = leftScreenLimit;
		
		else if (cameraPos.x > rightWorldLimit.x)
			gameObject.transform.position = rightScreenLimit;
		
		else if (cameraPos.y < upWorldLimit.y)
			gameObject.transform.position = UpScreenLimit;
		
		else if (cameraPos.y > downWorldLimit.y)
			gameObject.transform.position = DownScreenLimit;



	}
}
